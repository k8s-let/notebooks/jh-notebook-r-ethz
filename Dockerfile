FROM quay.io/jupyter/r-notebook:hub-4.0.2

USER root

# Fix timezone
RUN ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime && echo "Europe/Zurich" >> /etc/timezone

# Install ETHZ root ca certificates
ADD http://pkiaia.ethz.ch/aia/ETHZRootCA2020.pem /usr/local/share/ca-certificates/ETHZRootCA2020.crt
ADD http://pkiaia.ethz.ch/aia/ETHZIssuingCA2020.pem /usr/local/share/ca-certificates/ETHZIssuingCA2020.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootCA.pem /usr/local/share/ca-certificates/DigiCertGlobalRootCA.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertTLSRSASHA2562020CA1-1.pem /usr/local/share/ca-certificates/DigiCertTLSRSASHA2562020CA1-1.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalRootG2.pem /usr/local/share/ca-certificates/DigiCertGlobalRootG2.crt
ADD http://pkiaia.ethz.ch/aia/DigiCertGlobalG2TLSRSASHA2562020CA1.pem /usr/local/share/ca-certificates/DigiCertGlobalG2TLSRSASHA2562020CA1.crt
RUN chmod 644 /usr/local/share/ca-certificates/* && update-ca-certificates

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
  cargo \
  cmake \
  python3-pip \
  r-cran-rglpk \
  texlive-xetex texlive-lang-german texlive-science lmodern \
  libudunits2-dev \
  libmagick++-dev \
  libglpk-dev \
  libssl-dev \
  libgl1-mesa-glx \
  libglu1-mesa \
  libudunits2-dev \
  libxft2 \
  zip less vim man gpg povray xvfb \
  tesseract-ocr tesseract-ocr-eng tesseract-ocr-deu && \
  wget -nv https://download.owncloud.com/desktop/ownCloud/stable/latest/linux/Ubuntu_22.04/Release.key -O - | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/owncloud.gpg > /dev/null && \
  echo 'deb https://download.owncloud.com/desktop/ownCloud/stable/latest/linux/Ubuntu_22.04/ /' | sudo tee -a /etc/apt/sources.list.d/owncloud.list && \
  apt-get update && \
  apt-get install -y owncloud-client && \
  apt-get clean

RUN mamba install -q -c defaults -c conda-forge -c bioconda -c bokeh -c python-control \
  control \
  fqdn \
  isoduration \
  jupyterlab-git \
  nbgitpuller \
  uri-template \
  webcolors \
  r-ape \
  r-brms \
  r-bio3d \
  r-devtools \
  r-ggraph \
  r-igraph \
  r-lpsolve \
  r-lhs \
  r-rglpk \
  r-rsolnp \
  r-terra \
  r-units \
  && \
  mamba clean --all

RUN jupyter labextension disable "@jupyterlab/apputils-extension:announcements"

# R libs
#
RUN Rscript >/dev/null 2>&1 -e "install.packages(pkgs=c('BiocManager'), repos=c('http://cran.r-project.org'))" \
    && \
    Rscript >/dev/null 2>&1 -e "BiocManager::install()"
    
RUN Rscript >/dev/null 2>&1 -e "install.packages(pkgs=c( \
    'bayesplot', \
    'bayestestR', \
    'blockmodeling', \
    'biostat3', \
    'countrycode', \
    'GGally', \
    'ggforce', \
    'goldfish', \
    'gt', \
    'gtsummary', \
    'lfe', \
    'loo', \
    'matlib', \
    'mice', \
    'nloptr', \ 
    'RColorBrewer', \
    'RcppEigen', \
    'scico', \
    'sensitivity', \
    'statnet', \
    'sjPlot', \
    'sna', \
    'stargazer', \
    'tidybayes', \
    'tidygraph'), repos=c('http://cran.r-project.org'), dependencies=TRUE, timeout=300)"

RUN Rscript >/dev/null 2>&1 -e "install.packages(pkgs=c( \
    'manynet', \
    'migraph', \
    'RBGL', \
    'sbm'), repos=c('http://cran.r-project.org'), dependencies=TRUE, timeout=300)" \
    && \
    Rscript -e "remotes::install_github('schochastics/networkdata')"


USER 1000
